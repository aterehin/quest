var data = [
    {x: 0,  y: 0,  radius: 5},
    {x: 10, y: 10, radius: 10}
  ];

var server = sinon.fakeServer.create();

function reformatDate( date ) {
    return date.toISOString().replace(/.[^.]+$/, '');
}

function getRandomValue( min, max ) {
    return parseInt( Math.random() * (max - min) + min );
}

function generateData( start, end ) {
    var diff;
    var data = [];
    var now = new Date();
    var day = now.getDay();
    var sDate = new Date( start );
    var eDate = new Date( end );
    var cDate = new Date( start );

    if( eDate.getTime() > now.getTime() ) {
        diff = day == 0 ? 7 : day;
    } else {
        diff = parseInt( ( eDate.getTime() - sDate.getTime() ) / ( 24 * 3600 * 1000 ) ) + 1; 
    }

    for( let i = 0; i < diff; i++ ) {
        var dd = cDate.getDate();
        var count = getRandomValue( 0, 5 );

        if( count != 0 ) {
            for( let i = 0; i < count; i++ ) {
                var value = getRandomValue( -500, 500 );

                if( value != 0 ) {
                   data.push({
                        date: reformatDate( cDate ),
                        value: value
                    });
                }
            }
        }

        cDate.setDate( dd + 1 );
    }

    return data;
}

server.respondWith(
    /\/api\/transactions\/((?:\d{2,4}-?)+)\/((?:\d{2,4}-?)+)/, 
    function (xhr, start, end) {
        xhr.respond( 
            200, 
            { "Content-Type":"application/json" }, 
            JSON.stringify( generateData( start, end ) )
        ); 
    }
);

server.autoRespond = true;
