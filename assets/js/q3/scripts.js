var chart = (function(){

	'use strict';

	const API_URL = '/api/transactions';

	var current = new Date();
	const months = ["January", "February", "March", "April", "May", "June","July", 
					"August", "September", "October", "November", "December"];

	const options = {
		seriesBarDistance: 20,
		axisY: { 
			offset: 0,
			showLabel: false 
		},
		axisX: { 
			showGrid: false 
		}
	}

	const elements = {
		nav: {
			prev: document.querySelector('.graph_nav_item.__prev'),
			next: document.querySelector('.graph_nav_item.__next'),
			week: document.querySelector('.graph_nav_item.__label dt'),
			label: document.querySelector('.graph_nav_item.__label dd')
		}
	}

	const module = {

		chartist: new Chartist.Bar('.ct-chart', {}, options),

		getISODate: function( date ) {
			if( !(date instanceof Date) ) {
				date = new Date( date );
			}

			let dd = date.getDate();
			if (dd < 10) dd = '0' + dd;

			let mm = date.getMonth() + 1;
			if (mm < 10) mm = '0' + mm;

			let yyyy = date.getFullYear();

			return yyyy + '-' + mm + '-' + dd;
		},

		isCurrentWeek: function() {
			let now = new Date();
			let next = new Date( current );

			next.setDate( current.getDate() + 7 );

			return next.getTime() > now.getTime();
		},

		getWeek: function( d ) {
			let day = d.getDay();
			let year = d.getFullYear();
			let month = d.getMonth();
			let date = d.getDate();

			return [
				new Date(year, month, date + (day == 0 ? -6 : 1) - day ), 
				new Date(year, month, date + (day == 0 ? 0 : 7) - day )
			];
		},

		getWeekNumber: function( date ) {
			var start = new Date( date.getFullYear(), 0, 1 );

        	return Math.ceil( (( (date - start) / 86400000) + start.getDay() + 1 ) / 7 );
		},

		changeWeek: function( date ) {
			let range = module.getWeek( date );
			let url = module.getUrl( range );

			module.get( url, function( data ) {
				let _data = transformData( data, range );

				updateView( _data.chartist  );
			});
		},

		getUrl: function( dates ) {
			let url = API_URL;

			if( dates[0] instanceof Date && 
				dates[1] instanceof Date ) {
				url = url + '/' + module.getISODate( dates[0] ) + 
							'/' + module.getISODate( dates[1] );
			}

			return url ;
		},

		get: function( url, callback ) {
			let xhr = new XMLHttpRequest();

			xhr.onreadystatechange = function() { 
		        if ( xhr.readyState == 4 && xhr.status == 200 ) {
		            callback( JSON.parse( xhr.responseText ) );
		        }
		    }

			xhr.open( 'GET', url, true );
			xhr.send();
		},

		on: function( elem, event, callback ) {
			let _on = function( el ) {
				if ( el.addEventListener ) {
					el.addEventListener( event, callback );
				} else {
					el.attachEvent( 'on' + event, callback );
				}
			}

			if( elem instanceof Array ) {
				for( var i=0; i < elem.length; i++ ) {
		        	_on( elem[i] ); 
		        }
			} else {
				_on( elem );
			}
		} 
	}

	function init() {
		module.on( elements.nav.prev, 'click', handleClickPrev );
		module.on( elements.nav.next, 'click', handleClickNext );

		module.changeWeek( current );
	}

	function getEnding( num, endings ) {
		let ending, i;

		num = num % 100;

		if ( num >= 11 && num <= 19 ) {
			ending = endings[3];
		} else {
			i = num % 10;

			switch (i) {
				case (1): ending = endings[0]; break;
				case (2): ending = endings[1]; break;
				case (3): ending = endings[2]; break;
				default: ending = endings[3]; break;
			}
		}

		return ending;
	}

	function updateView( data ) {
		let days = module.getWeek( current );
		let month = months[days[0].getMonth()];
		let year = current.getFullYear();
		let week = module.getWeekNumber( current );
		let ending = getEnding( week, ['st', 'nd', 'rd', 'th'] );

		if( module.isCurrentWeek() ) {
			week = 'This';
		} else {
			week = week + ending;
		}

		elements.nav.week.innerHTML = week + ' week';
		elements.nav.label.innerHTML = month + ' ' + days[0].getDate() + '-' + days[1].getDate() + ', ' + year;

		module.chartist.update( data, options );
	}

	function transformData( data, range ) {
		let _data = {
			chartist: {
				labels: [],
				series: [[],[]]
			}
		}

		for( let i = 0; i < data.length; i++ ) {
			let date = module.getISODate( data[i]['date'] );
			let value = data[i]['value'];

			if( !_data[date] ) {
				_data[date] = {};
			}

			if( typeof _data[date]['expense'] === 'undefined' ) {
				_data[date]['expense'] = value < 0 ? -value : 0;
			} else {
				_data[date]['expense'] = (value < 0 ? -value : 0) + _data[date]['expense'];
			}

			if( typeof _data[date]['income'] === 'undefined' ) {
				_data[date]['income'] = value < 0 ? 0 : value;
			} else {
				_data[date]['income'] = (value < 0 ? 0 : value) + _data[date]['income'];
			}
		}

		let sDate = new Date( range[0] );
    	let eDate = new Date( range[1] );
    	let cDate = new Date( range[0] );
    	let diff = parseInt( ( eDate.getTime() - sDate.getTime() ) / ( 24 * 3600 * 1000 ) );

    	for( let i = 0; i < diff + 1; i++ ) {
    		let date = module.getISODate( cDate );
    		let day = cDate.getDate();

    		_data.chartist['labels'].push( day.toString() );

    		if( typeof _data[date] === 'undefined' ) {
    			_data.chartist['series'][0].push( 0 );
    			_data.chartist['series'][1].push( 0 );
    		} else {
    			_data.chartist['series'][0].push( _data[date]['expense'] );
    			_data.chartist['series'][1].push( _data[date]['income'] );
    		}

    		cDate.setDate( day + 1 );
    	}

		return _data;
	}

	function handleClickPrev() {
		current.setDate( current.getDate() - 7 );
		module.changeWeek( current );
	}

	function handleClickNext() {
		if( !module.isCurrentWeek() ) {
			current.setDate( current.getDate() + 7 );
			module.changeWeek( current );
		}
	}

	init();

	return module;

})();

