(function(){

	console.log( 'ES5 Example:' );

	var arr = [];

	for ( var x = 0; x < 5; x++ ) {
		var fn = (function( _x ) {
	    	return function() { console.log( _x ) }
	    })( x );

	    arr.push( fn );
	}

	for (var idx = 0; idx < arr.length; idx++) {
	    arr[idx]();
	}
})();


(() => {
	'use strict';

	console.log( 'ES6 Example:' );

	const arr = [];

	for ( let x = 0; x < 5; x++ ) {
	    arr.push( () => console.log( x ) );
	}

	for ( let x = 0; x < arr.length; x++ ) {
	    arr[x]();
	}
})();


