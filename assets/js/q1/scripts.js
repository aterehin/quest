(function () {
    if(document.all){
        return;
    }

    var all = function (id) {
        return document.getElementsByTagName('*')[id];
    };

    if (window.Proxy) {
        all = new Proxy(all, {
            get: function (target, property) {
                return target(property);
            }
        });
    }

    document.all = all;
})();